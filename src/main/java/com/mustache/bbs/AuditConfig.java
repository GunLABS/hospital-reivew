package com.mustache.bbs;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
public class AuditConfig { 
    //test 코드에서 에러가 발생하여 BbsApplication에서 분리하여 AuditConfig 클래스 생성 후
    //@EnableJpaAuditing 적용
}
